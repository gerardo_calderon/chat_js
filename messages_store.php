<?
include_once('resources/session.php');
include_once('resources/connection.php');
mysql_select_db($bd,$connection);

$pedido = $_REQUEST["pedido"];

switch($pedido){
	case 'contactos':
		$empleados = "";
		$selEmp = "SELECT id_empleado,CONCAT(nombres,' ',apellidos) AS 'empleado',picture FROM empleados WHERE id_empleado <> {$_SESSION["id_usr"]} ORDER BY nombres,apellidos";
		
		$res = mysql_query($selEmp,$connection);
		if(mysql_num_rows($res)>0){
			while($iE=mysql_fetch_array($res)){
				$imagen = "img/noPicture.png";
				if($iE["picture"]!="" && file_exists("pictures/".$iE["picture"])) $imagen = "pictures/".$iE["picture"];

				$empleados .= getBodyContact($iE["id_empleado"],$imagen,utf8_encode($iE["empleado"]),"0");
			}
		}
		
		echo $empleados;

	break;

	case 'mensajes':
		if(!isset($_POST["empleado"])) exit();
		$idDueno = $_SESSION["id_usr"];
		$idInvit = addslashes($_POST["empleado"]);
		$mensajes="";
		$tipoA="";
		$format = $_conf["formato_fecha_mysql"];

		$selMensajes = "SELECT * FROM(
				(SELECT CONCAT(e.nombres,' ',e.apellidos) AS 'empleado', e.picture, ms.id_salida AS 'id',ms.mensaje,
				DATE_FORMAT(ms.creacion,'{$format} %h:%i %p') AS 'fecha','enviado' AS 'tipo', ms.creacion
				FROM msg_salida AS ms INNER JOIN empleados AS e ON ms.id_emisor = e.id_empleado
				WHERE ms.id_emisor = {$idDueno} AND ms.id_receptor = {$idInvit})
				UNION ALL
				(SELECT CONCAT(e.nombres,' ',e.apellidos) AS 'empleado', e.picture, me.id_entrada AS 'id', me.mensaje,
				DATE_FORMAT(me.creacion,'{$format} %h:%i %p') AS 'fecha','recibido' AS 'tipo', me.creacion
				FROM msg_entrada AS me INNER JOIN empleados AS e ON me.id_emisor = e.id_empleado
				WHERE me.id_receptor = {$idDueno} AND me.id_emisor = {$idInvit})
				) AS t ORDER BY creacion";
		
		$res = mysql_query($selMensajes,$connection);
		if(mysql_num_rows($res)>0){
			while($iM=mysql_fetch_array($res)){
				$imagen = "img/noPicture.png";
				$fecha = $iM["fecha"];
				if($iM["picture"]!="" && file_exists("pictures/".$iM["picture"])) $imagen = "pictures/".$iM["picture"];
				$seguido = ($iM["tipo"]!=$tipoA)?false:true;
				$tipoA=$iM["tipo"];
				$mensajes .= getBodyMessage($imagen,utf8_encode($iM["empleado"]),$fecha,utf8_encode($iM["mensaje"]),$iM["id"],$iM["tipo"],$seguido);
			}
		}else{
			$mensajes = "<table width='100%' height='100%'><tr><td style='text-align:center' valign='middle'><div class='noMessages'>No Messages</div></td></tr></table>";;
		}

		$updtMsg = "UPDATE msg_entrada SET leido = 'true' WHERE id_receptor = {$idDueno} AND id_emisor = {$idInvit}";
		mysql_query($updtMsg,$connection);

		echo $mensajes;

	break;

	case 'enviar':

		if(!isset($_POST["empleado"]) || !isset($_POST["texto"])) exit();
		$idDueno = $_SESSION["id_usr"];
		$idInvit = addslashes($_POST["empleado"]);
		$mensaje = utf8_decode(addslashes($_POST["texto"]));

		$nuevoMensaje = "INSERT INTO msg_salida(id_emisor,id_receptor,mensaje,creacion) VALUES({$idDueno},{$idInvit},'{$mensaje}',NOW())";
		$res = mysql_query($nuevoMensaje,$connection);

		if($res){ echo "{success:true}"; }
		else{ echo "{success:false}"; }

	break;

	case 'revisar':
		$idDueno = $_SESSION["id_usr"];
		$selMensajes = "SELECT me.id_emisor, COUNT(me.id_entrada) AS 'total' FROM msg_entrada AS me
						WHERE me.id_receptor = {$idDueno} AND me.leido = 'false' GROUP BY me.id_emisor";
		$res = mysql_query($selMensajes,$connection);
		$mensajes = array();
		while($iM = mysql_fetch_array($res)){
			if($iM["total"]!="0"){
				$mensajes[]=array("id"=>$iM["id_emisor"],"total"=>$iM["total"]);
			}
		}
		echo json_encode($mensajes);

	break;

}

function getNewMessages($receptor,$emisor){
	return "";
}

?>