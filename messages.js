
Ext.onReady(function(){
	//Ext.get('title_contact_box').on('click',function(){ messages.toggleContacts(); });
	messages.init();
});

var messages = {
	nameObject: 'messages',
	idReceiver:0,
	stateContact:false,
	stateMessages:false,
	idTitle:'title_contact_box',
	idListContacts:'list_contact_box',
	idListMessages:'list_messages_box',
	refreshTime:5000,
	audioPath:'img/notify',

	toggleContacts:function(){
		var _t = this;
		if(_t.stateContact){
			Ext.get(_t.idListContacts).slideOut('b', { useDisplay:true });
			_t.stateContact=false;
		}else{
			Ext.get(_t.idListContacts).slideIn('t',{ useDisplay:true });
			_t.stateContact=true;
		}
	},

	toggleMessages:function(){
		var _t = this;
		if(_t.stateMessages){
			document.getElementById('typer_message').style.display="none";
			Ext.get(_t.idListMessages).slideOut('b', { useDisplay:true });
			_t.stateMessages=false;
		}else{
			document.getElementById('typer_message').style.display="block";
			Ext.get(_t.idListMessages).slideIn('t',{ useDisplay:true });
			_t.stateMessages=true;
		}
	},

	init:function(){
		var _t = this;
		_t.setupAudio();
		if(_t.showContactsBox()){
			_t.loadContacts(function(res){
				document.getElementById(_t.idListContacts).innerHTML = res.responseText;
				_t.pullChanges();
				setInterval(_t.nameObject+".pullChanges()",_t.refreshTime);
			});
		}
		
	},

	loadContacts:function(success){
		Ext.Ajax.request({
			url:'messages_store.php', params:{pedido:'contactos'}, method:'POST',
			success:function(response,opts){
				if(typeof(success)=='function') success(response);
			},
			failure:function(){ }
		});
	},

	loadMessages:function(id,success){
		Ext.Ajax.request({
			url:'messages_store.php', params:{pedido:'mensajes',empleado:id}, method:'POST',
			success:function(response,opts){
				if(typeof(success)=='function') success(response);
			},
			failure:function(){ }
		});
	},

	sendMessage:function(text,success){
		var _t = this;
		Ext.Ajax.request({
			url:'messages_store.php', params:{pedido:'enviar',empleado:_t.idReceiver,texto:text}, method:'POST',
			success:function(response,opts){
				if(typeof(success)=='function') success(response);
			},
			failure:function(){ }
		});
	},

	showMessagesEmployee:function(id){
		var _t = this;
		_t.idReceiver=id;
		if(!document.getElementById('nameE_'+id)) return false;
		if(_t.showMessagesBox()){
			var nameEmployee = document.getElementById('nameE_'+id).value;
			document.getElementById('title_messages_box').innerHTML = nameEmployee;
			_t.loadMessages(id,function(res){
				document.getElementById(_t.idListMessages).innerHTML = res.responseText;
				document.getElementById(_t.idListMessages).scrollTop = document.getElementById(_t.idListMessages).scrollHeight;

				document.getElementById('text_employee_'+id).innerHTML = nameEmployee;
				_t.totalMessages -= document.getElementById('text_employee_'+id).getAttribute('numMessages');
				document.getElementById('text_employee_'+id).setAttribute('numMessages',0);
				_t.notifyGlobalChanges();
			});
		}
	},

	showContactsBox:function(){
		if(!document.getElementById('body_wrapper')) return;
		if(document.getElementById('title_contact_box')) return false;
		var _t = this;
		var wrapper = document.getElementById('body_wrapper');
		var div = document.createElement('div');
		div.id = "contacts_box";
		div.className="contactBox";
		div.innerHTML = "<span id='title_contact_box' onClick='"+_t.nameObject+".toggleContacts()' class='titleContactBox'>Users</span> <div id='list_contact_box' class='contactListBox'> &nbsp; </div>";
		wrapper.appendChild(div);
		
		return true;
	},

	showMessagesBox:function(){
		if(!document.getElementById('body_wrapper')) return false;
		var _t = this;
		if(document.getElementById('messages_box')){
			if(!_t.stateMessages) _t.toggleMessages();
			return true;	
		}
		var _t = this;
		var wrapper = document.getElementById('body_wrapper');
		var div = document.createElement('div');
		div.id = "messages_box";
		div.className="messagesBox";
		div.innerHTML = "<span id='title_messages_box' onClick='"+_t.nameObject+".toggleMessages()' class='titleMessagesBox'>&nbsp;</span> "+
						"<div id='list_messages_box' class='messagesListBox'> &nbsp; </div>"+
						"<input type='text' class='typerMessage' id='typer_message' placeHolder='Press ENTER to send' onKeyPress='"+_t.nameObject+".enterListener(event)' />";
		_t.stateMessages=true;
		wrapper.appendChild(div);

		return true;
	},

	enterListener:function(e){
		var tecla = (document.all) ? e.keyCode : e.which;
		if(tecla==13){
			var typer = document.getElementById('typer_message');
			if(typer.value!=""){
				typer.disabled="disabled";
				messages.sendMessage(typer.value,function(){
					typer.disabled="";
					typer.value="";
					messages.showMessagesEmployee(messages.idReceiver);
				});
			}
		}
	},

	pullChanges:function(){
		Ext.Ajax.request({
			url:'messages_store.php', params:{pedido:'revisar'}, method:'POST',
			success:function(response,opts){
				var T = Ext.decode(response.responseText);
				var total = 0;
				if(T.length>0){
					for(var i=0;i<T.length;i++){
						if(document.getElementById('text_employee_'+T[i].id)){
							var nameE = document.getElementById('nameE_'+T[i].id).value;
							document.getElementById('text_employee_'+T[i].id).innerHTML = nameE + " ("+T[i].total+")";
							document.getElementById('text_employee_'+T[i].id).setAttribute('numMessages',T[i].total);
							total += parseInt(T[i].total);
							if(messages.idReceiver==T[i].id){
								messages.showMessagesEmployee(T[i].id);
							}
						}
					}
				}
				if(messages.totalMessages!=total&&total>0){ messages.playAudio(); }
				messages.totalMessages = total;
				messages.notifyGlobalChanges();
			},
			failure:function(){ }
		});
	},

	notifyGlobalChanges:function(){
		if(messages.totalMessages>0){
			document.getElementById('title_contact_box').innerHTML = 'Users ('+messages.totalMessages+')';
		}else{
			document.getElementById('title_contact_box').innerHTML = 'Users';
		}
	},

	setupAudio:function(){
		var _t = this;
		var audio = document.createElement('div');
		audio.style.display="none";
		audio.innerHTML = '<audio id="notifySound"> <source src="'+_t.audioPath+'.ogg" type="audio/ogg"> <source src="'+_t.audioPath+'.mp3" type="audio/mpeg"> </audio>';
		var wrapper = document.getElementById('body_wrapper').appendChild(audio);
	},

	playAudio:function(){
		var sound = document.getElementById('notifySound');
		sound.play();
	}

}

